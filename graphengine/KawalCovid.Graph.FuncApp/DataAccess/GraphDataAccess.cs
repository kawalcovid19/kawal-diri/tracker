﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Gremlin.Net.Driver;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Gremlin.Net.Structure.IO.GraphSON;

namespace KawalCovid.Graph.FuncApp.DataAccess
{
    public class GraphDataAccess
    {
        private readonly string m_Hostname;
        private static int m_Port = 443;
        private readonly string m_AuthKey;
        private readonly string m_Database;
        private readonly string m_Collection;
        private readonly ILogger m_Logger;

        public GraphDataAccess(ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException("logger cannot be null");

            m_Hostname = Environment.GetEnvironmentVariable("hostname");
            m_Database = Environment.GetEnvironmentVariable("database");
            m_Collection = Environment.GetEnvironmentVariable("collection");
            m_AuthKey = Environment.GetEnvironmentVariable("authkey");
            m_Logger = logger;
        }

        public GremlinClient InstantiateGremlinClient()
        {
            var gremlinServer = new GremlinServer(m_Hostname, m_Port, enableSsl: true,
                                                  username: "/dbs/" + m_Database + "/colls/" + m_Collection,
                                                  password: m_AuthKey);

            GremlinClient result = new GremlinClient(gremlinServer, new GraphSON2Reader(), new GraphSON2Writer(), GremlinClient.GraphSON2MimeType);

            return result;
        }

        public Task<ResultSet<dynamic>> SubmitRequest(GremlinClient gremlinClient, string query)
        {
            try
            {
                return gremlinClient.SubmitAsync<dynamic>(query);
            }
            catch (Exception e)
            {
                m_Logger.LogInformation(e, "Kawalcovid19 Graph SubmitAsync exception: " + query);

                throw;
            }
        }
    }
}
