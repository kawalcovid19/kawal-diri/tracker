﻿using Xunit;
using KawalCovid.Graph.BatchFuncApp.Loader;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.BatchFuncApp.Loader.Tests
{
    public class GraphLoaderTests
    {
        [Fact()]
        public void LoadGraphTest()
        {
            GraphLoader loader = new GraphLoader();
            loader.RawDataUrl = "https://kawalcovid19.blob.core.windows.net/analytics-dataset/data/raw/daftar_positif_kemenkes.csv?sp=r&st=2020-03-25T11:25:36Z&se=2025-03-25T19:25:36Z&spr=https&sv=2019-02-02&sr=b&sig=ywPOqa1baVN05bo0MNMZIo6PIlGncdQdk%2Fow8oLLRGI%3D";
            loader.PatientRestApiUrl = "https://stgkawaldirifuncapp.azurewebsites.net/api/PatientVertexFunction?code=p/KgGJ6XExhpxpCrmgya8erVxoggT32DgYpf0YcQcN97z18tGroVbg==";
            loader.ClusterRestApiUrl = "https://stgkawaldirifuncapp.azurewebsites.net/api/ClusterVertexFunction?code=y3DvlbD9CSu9K1elKnPfhRq3a9t8NOAsmFaQ65a/CJPhxCoL2XjLrQ==";
            loader.LoadGraph();
        }
    }
}