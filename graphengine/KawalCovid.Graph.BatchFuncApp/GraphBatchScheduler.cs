using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using CsvHelper;
using CsvHelper.Configuration;
using KawalCovid.Graph.BatchFuncApp.Loader;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace KawalCovid.Graph.BatchFuncApp
{
    public static class GraphBatchScheduler
    {
        [FunctionName("GraphScheduler")]
        public static void Run([TimerTrigger("0 */5 * * * *")]TimerInfo timerInfo, ILogger log)
        {
            GraphLoader loader = new GraphLoader();
            loader.Log = log;

            loader.LoadGraph();
        }
    }
}
