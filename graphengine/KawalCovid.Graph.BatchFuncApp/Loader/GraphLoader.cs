﻿using CsvHelper;
using CsvHelper.Configuration;
using KawalCovid.Graph.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace KawalCovid.Graph.BatchFuncApp.Loader
{
    public class GraphLoader
    {
        private string _RawDataUrl;
        private string _ClusterRestApiUrl;
        private string _patientRestApiUrl;

        public string RawDataUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_RawDataUrl))
                {
                    _RawDataUrl = Environment.GetEnvironmentVariable("");
                }

                return _RawDataUrl;
            }
            set
            {
                _RawDataUrl = value;
            }
        }

        //patientRestApiUrl

        public string ClusterRestApiUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_ClusterRestApiUrl))
                {
                    _ClusterRestApiUrl = Environment.GetEnvironmentVariable("clusterRestApiUrl");
                }

                return _ClusterRestApiUrl;
            }
            set
            {
                _ClusterRestApiUrl = value;
            }
        }

        public string PatientRestApiUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_patientRestApiUrl))
                {
                    _patientRestApiUrl = Environment.GetEnvironmentVariable("patientRestApiUrl");
                }

                return _patientRestApiUrl;
            }
            set
            {
                _patientRestApiUrl = value;
            }
        }

        public ILogger Log { get; set; }

        public void LoadGraph()
        {
            //download .csv file raw data
            string url = this.RawDataUrl;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                CsvConfiguration configuration = new CsvConfiguration(CultureInfo.CurrentCulture)
                {
                    Delimiter = ",",
                    HasHeaderRecord = true
                };

                using (CsvReader csvReader = new CsvReader(streamReader, configuration))
                {
                    using (var dr = new CsvDataReader(csvReader))
                    {
                        var dt = new DataTable();
                        dt.Load(dr);

                        //prepare vertices
                        //Cluster
                        List<Cluster> clusters = GetClusters(dt);

                        //prepare patient
                        List<Patient> patients = GetPatients(dt);

                        //ingest cluster
                        IngestClusters(clusters);

                        //ingest patient
                        IngestPatients(patients);

                        //create edges
                        //TODO: pending better data source
                        IngestPatientClusterEdges(dt, patients, clusters);
                        IngestPatientPatientEdges(dt, patients);

                    }
                }
            }
        }

        private void IngestPatientPatientEdges(DataTable dt, List<Patient> patients)
        {
            //{ "sourceVertexType": "person", "sourceId": "per-123", "edgeLabel": "visited", "targetId": "pla-567" }

            string url = "https://stgkawaldirifuncapp.azurewebsites.net/api/GenericEdgeFunction?code=Hhma4E/aO3ayHff03ziaW0Y0/jJumY6oQcmTdVHfYi7T8hOgaXIQfQ==";

            DataView view = new DataView(dt);
            view.RowFilter = "source <> ''";
            DataTable filteredRows = view.ToTable();

            foreach (DataRow item in filteredRows.Rows)
            {
                //
                string sourcePatient = "pat-" + item["Source"].ToString();
                string targetPatient = "pat-" + item["No"].ToString();
                string jsonContent = "{ \"sourceVertexType\": \"patient\", \"sourceId\": \"" + sourcePatient + "\", \"edgeLabel\": \"related_to\", \"targetId\": \"" + targetPatient + "\" }";
                this.IngestEdges(url, jsonContent);
            }
        }

        private void IngestPatientClusterEdges(DataTable dt, List<Patient> patients, List<Cluster> clusters)
        {
            string url = "https://stgkawaldirifuncapp.azurewebsites.net/api/GenericEdgeFunction?code=Hhma4E/aO3ayHff03ziaW0Y0/jJumY6oQcmTdVHfYi7T8hOgaXIQfQ==";

            foreach (DataRow item in dt.Rows)
            {
                //
                string source = "clu-" + item["klasterid"].ToString();
                string target = "pat-" + item["No"].ToString();
                string jsonContent = "{ \"sourceVertexType\": \"cluster\", \"sourceId\": \"" + source + "\", \"edgeLabel\": \"transmitted_from\", \"targetId\": \"" + target + "\" }";
                this.IngestEdges(url, jsonContent);
            }
        }

        private void IngestPatients(List<Patient> patients)
        {
            //ingest patient
            foreach (Patient patient in patients)
            {
                // Json to post.
                string jsonToSend = JsonConvert.SerializeObject(patient);

                IngestVertex(PatientRestApiUrl, patient, jsonToSend);
            }
        }
        private void IngestClusters(List<Cluster> clusters)
        {
            //ingest cluster
            foreach (Cluster cluster in clusters)
            {
                // Json to post.
                string jsonToSend = JsonConvert.SerializeObject(cluster);

                IngestVertex(ClusterRestApiUrl, cluster, jsonToSend);
            }
        }

        private async void IngestVertex(string url, IGraphVertex vertex, string jsonToSend)
        {
            var client = new RestClient(url);
            var req = new RestRequest(url, Method.POST);
            req.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            req.RequestFormat = DataFormat.Json;

            try
            {
                var cancellationTokenSource = new CancellationTokenSource();
                var restResponse = await client.ExecuteAsync(req, cancellationTokenSource.Token);
            }
            catch (Exception error)
            {
                if (Log != null)
                {
                    Log.LogError("Vertex ID" + vertex.ID + " - " + error.Message, error);
                }
            }
        }

        private async void IngestEdges(string url, string jsonToSend)
        {
            var client = new RestClient(url);
            var req = new RestRequest(url, Method.POST);
            req.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            req.RequestFormat = DataFormat.Json;

            try
            {
                var cancellationTokenSource = new CancellationTokenSource();
                var restResponse = await client.ExecuteAsync(req, cancellationTokenSource.Token);
            }
            catch (Exception error)
            {
                if (Log != null)
                {
                    Log.LogError("Edge: " + jsonToSend);
                }
            }
        }

        private List<Patient> GetPatients(DataTable dt)
        {
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "No", "Provinsi", "Usia", "JK", "Status", "WN");
            List<Patient> result = new List<Patient>();

            foreach (DataRow item in distinctValues.Rows)
            {
                int age = 0;
                Int32.TryParse(item["Usia"].ToString(), out age);

                result.Add(new Patient()
                {
                    ID = "pat-" + item["No"].ToString(),
                    Province = item["Provinsi"].ToString(),
                    CaseStatus = item["Status"].ToString(),
                    Age = age,
                    Sex = item["JK"].ToString(),
                    Nationality = item["WN"].ToString(),

                });
            }

            return result;
        }

        private List<Cluster> GetClusters(DataTable dt)
        {
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "klasterid", "Provinsi");
            List<Cluster> result = new List<Cluster>();

            foreach (DataRow item in distinctValues.Rows)
            {
                result.Add(new Cluster()
                {
                    ID = "clu-" + item["klasterid"].ToString(),
                    Province = item["Provinsi"].ToString()
                });
            }

            return result;
        }
    }
}
