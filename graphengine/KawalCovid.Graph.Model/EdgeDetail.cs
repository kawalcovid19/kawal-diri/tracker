﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.Model
{
    public class EdgeDetail
    {
        [JsonProperty(PropertyName = "sourceVertexType")]
        public string SourceVertexType { get; set; }

        [JsonProperty(PropertyName = "sourceId")]
        public string SourceID { get; set; }

        [JsonProperty(PropertyName = "edgeLabel")]
        public string EdgeLabel { get; set; }

        [JsonProperty(PropertyName = "targetId")]
        public string TargetID { get; set; }
    }
}
