﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.Model
{
    public class Patient : IGraphVertex
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "caseNumber")]
        public string CaseNumber { get; set; }

        [JsonProperty(PropertyName = "caseStatus")]
        public string CaseStatus { get; set; }

        [JsonProperty(PropertyName = "createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty(PropertyName = "updatedDate")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty(PropertyName = "diagnosedDate")]
        public DateTime DiagnosedDate { get; set; }

        [JsonProperty(PropertyName = "dischargeDate")]
        public DateTime DischargeDate { get; set; }

        [JsonProperty(PropertyName = "deceasedDate")]
        public DateTime DeceasedDate { get; set; }

        [JsonProperty(PropertyName = "sex")]
        public string Sex { get; set; }

        [JsonProperty(PropertyName = "age")]
        public int Age { get; set; }

        [JsonProperty(PropertyName = "nationality")]
        public string Nationality { get; set; }

        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "province")]
        public string Province { get; set; }
    }
}
