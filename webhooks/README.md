# Kawal Diri Webhooks

All webhooks are deployed to Azure Function.

## Setup

You need to install the following tools:

- Python 3
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
- [Azure Functions Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local)

Then clone the repository:

    git clone https://gitlab.com/kawalcovid19/kawal-diri/kawal-diri.git
    cd kawal-diri/webhooks

Create new virtual environment:

    python3 -m venv venv

Activate the virtual environment:

    # Unix
    source venv/bin/activate

    # Windows (Powershell)
    .\venv\Scripts\activate.bat

Install the dependencies:

    (venv) % pip install -r requirements.txt

If you are using VSCode: the linter, formatter & static type checker will be
enabled by default.

To setup the deployment, login to your azure account:

    az login

Then fetch the `local.settings.json` using the following command:

    func azure functionapp fetch-app-settings kawal-diri-webhooks


## Run

To run the webhooks locally, use the following command:

    func start

## Deploy

To deploy the webhooks, run the following command:

    func azure functionapp publish kawal-diri-webhooks

## Docs
- [Azure Functions developers guide](https://docs.microsoft.com/en-us/azure/azure-functions/functions-reference)

