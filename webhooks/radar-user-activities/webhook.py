# Convert raw events to Kawal Diri Graph
import azure.functions as func
import requests


def main(documents: func.DocumentList) -> func.HttpResponse:
    for doc in documents:
        # Get the data
        person_id = doc["event"]["user"]["userId"]
        event_type = doc["event"]["type"]
        place_id = None
        if "geofence" in event_type:
            place_id = doc["event"]["geofence"]["externalId"]
        if "place" in event_type:
            place_id = doc["event"]["palce"]["facebookId"]

        # Create the edge
        requests.post(
            "https://stgkawaldirifuncapp.azurewebsites.net/api/GenericEdgeFunction?code=Hhma4E/aO3ayHff03ziaW0Y0/jJumY6oQcmTdVHfYi7T8hOgaXIQfQ==",  # noqa
            json={
                "sourceVertexType": "person",
                "sourceId": person_id,
                "edgeLabel": "visited",
                "targetId": place_id,
            },
        )

    return func.HttpResponse(body={"message": "OK"}, status_code=200)
