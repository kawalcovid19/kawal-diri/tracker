# Transport raw events from radar.io to Azure Cosmos DB
# We store raw events in order to perform backfill, if something bad happen
import hashlib
import hmac
from os import environ as env
from typing import Any, Dict, Optional

from azure.cosmos import CosmosClient
from azure.functions import HttpRequest, HttpResponse

# Radar.io security token
# https://radar.io/dashboard/integrations?project=5e6cdc3401e56501fd88b7be&live=true
RADAR_SECURITY_TOKEN = env.get("RADAR_SECURITY_TOKEN", None)
# Azure account url & account key
AZURE_COSMOS_ACCOUNT_URL = env.get("AZURE_COSMOS_ACCOUNT_URL", None)
AZURE_COSMOS_ACCOUNT_KEY = env.get("AZURE_COSMOS_ACCOUNT_KEY", None)

# Configure Azure Cosmos DB client
if AZURE_COSMOS_ACCOUNT_KEY is None or AZURE_COSMOS_ACCOUNT_URL is None:
    raise ValueError("AZURE_COSMOS_ACCOUNT_* is not configured")
cosmos = CosmosClient(
    AZURE_COSMOS_ACCOUNT_URL, credential=AZURE_COSMOS_ACCOUNT_KEY
)
cosmos_db = cosmos.get_database_client("radar")
cosmos_container = cosmos_db.get_container_client("raw_events")

# Radar event type alias
RadarEvent = Dict[str, Any]


# Get event from radar, it returns None if request is not made from radar
# or the event is not available
def get_event_from_radar(req: HttpRequest) -> Optional[RadarEvent]:
    # Verify event from radar; if not returns None immediately
    # Get the security token from the header
    security_token = req.headers.get("X-Radar-Signature", None)
    if security_token is None:
        return None

    # Get the expected security token
    data = req.get_json()
    event = data.get("event", None)
    if event is None:
        return None

    # Generate expected security token
    if RADAR_SECURITY_TOKEN is None:
        raise ValueError("RADAR_SECURITY_TOKEN is not specified")
    key = RADAR_SECURITY_TOKEN.encode("utf-8")
    event_id = event.get("_id", None)
    if event_id is None:
        return None
    message = event_id.encode("utf-8")
    expected_security_token = hmac.new(key, message, hashlib.sha1).hexdigest()

    # Check wether the security token is correct
    if not hmac.compare_digest(security_token, expected_security_token):
        return None

    # Otherwise returns the event as JSON formatted string
    return data


def main(req: HttpRequest) -> HttpResponse:
    # Get the event
    event = get_event_from_radar(req)
    if event is None:
        return HttpResponse(
            body="Request is not from radar.io or events is not found",
            status_code=400,
        )

    # Transport the event to Azure CosmosDB
    # Use user_id as partition key
    item = event
    partition_key = "user_{}".format(event["user"]["_id"],)
    item["partition_key"] = partition_key

    cosmos_container.upsert_item(item)
    return HttpResponse(body="OK", status_code=200)
