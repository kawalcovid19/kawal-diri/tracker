# Kawal Diri
See [project goals and specification here](https://kawalcovid19.quip.com/yYikAVfbUCsB/Product-SSOT-Kawal-COVID-19 )

## Architecture
![](docs/architecture.png)

### Graph Design
![](docs/graph_design.png)

### STG REST APIs (Function App)
This is meant for references only. REST APIs will be published through API-M

API URLs (STG):
Cluster Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/ClusterVertexFunction?code=y3DvlbD9CSu9K1elKnPfhRq3a9t8NOAsmFaQ65a/CJPhxCoL2XjLrQ==

Event Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/EventVertexFunction?code=X2dU6FHe9TCFDVOtlucCUBRqbwZiOCBuEjtX3pWYCZaEBHqlTDsJSg==

Hospital Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/HospitalVertexFunction?code=WJqXTI7WoiNC137mufFpKmoqeU1dcI25J5wiWuavnx588xwbgQzBFA==

Patient Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/PatientVertexFunction?code=p/KgGJ6XExhpxpCrmgya8erVxoggT32DgYpf0YcQcN97z18tGroVbg==

Person Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/PersonVertexFunction?code=SnqUVahOm2wrj0GAvl4e2rOnLYQW7De2ameJpaGeHP6tshefQizWBQ==

Place Vertex:
https://stgkawaldirifuncapp.azurewebsites.net/api/PlaceVertexFunction?code=i7MQ0Hc5atiY7B5eBzWSgnmyzK6d6raPHrSNfaJzwvTXyViyZdFRAg==

Generic Edge Processor:
https://stgkawaldirifuncapp.azurewebsites.net/api/GenericEdgeFunction?code=Hhma4E/aO3ayHff03ziaW0Y0/jJumY6oQcmTdVHfYi7T8hOgaXIQfQ==